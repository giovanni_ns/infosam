<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231114124231 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE endereco ALTER cep TYPE VARCHAR(8)');
        $this->addSql('ALTER TABLE pessoa ALTER cpf TYPE VARCHAR(11)');
        $this->addSql('ALTER TABLE pessoa ALTER rg TYPE VARCHAR(9)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE endereco ALTER cep TYPE VARCHAR(9)');
        $this->addSql('ALTER TABLE pessoa ALTER cpf TYPE VARCHAR(14)');
        $this->addSql('ALTER TABLE pessoa ALTER rg TYPE VARCHAR(12)');
    }
}
