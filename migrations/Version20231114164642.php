<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231114164642 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE usuario ADD nome VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE usuario ADD setor VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE usuario ADD cargo VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE usuario ALTER role DROP DEFAULT');
        $this->addSql('ALTER TABLE usuario ALTER ativo DROP DEFAULT');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE usuario DROP nome');
        $this->addSql('ALTER TABLE usuario DROP setor');
        $this->addSql('ALTER TABLE usuario DROP cargo');
        $this->addSql('ALTER TABLE usuario ALTER role SET DEFAULT 0');
        $this->addSql('ALTER TABLE usuario ALTER ativo SET DEFAULT false');
    }
}
