<?php

namespace App\Model;

class ContatoDto
{
    public function __construct(
        public string $telefone_residencial,
        public string $telefone_celular,
        public string $telefone_outro,
        public string $email
    ) {
    }
}
