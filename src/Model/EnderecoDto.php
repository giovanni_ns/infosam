<?php

namespace App\Model;

class EnderecoDto
{
    public function __construct(
        public string $cep,
        public string $bairro,
        public string $logradouro,
        public string $municipio,
        public string $uf
    ) {
    }
}
