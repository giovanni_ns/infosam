<?php

namespace App\Model;

class UsuarioLoginDto
{
    public function __construct(
        public string $apelido,
        public string $senha
    ) { }
}
