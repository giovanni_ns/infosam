<?php

namespace App\Model;

class UsuarioCadastroDto
{
    public function __construct(
        public string $nome,
        public string $setor,
        public string $cargo,
        public string $email,
        public string $apelido,
        public string $senha,
    ) { }
}
