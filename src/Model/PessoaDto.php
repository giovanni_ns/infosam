<?php

namespace App\Model;

class PessoaDto
{
    public function __construct(
        public string $nome,
        public string $data_nascimento,
        public string $cpf,
        public string $rg,
        public string $rg_orgao_emissor,
        public string $rg_uf,
        public bool $sexo,
        public string $complemento,
        public int $numero,
        public string $estado_civil
    ) {
    }
}
