<?php

namespace App\Repository;

use App\Entity\Policial;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Policial>
 *
 * @method Policial|null find($id, $lockMode = null, $lockVersion = null)
 * @method Policial|null findOneBy(array $criteria, array $orderBy = null)
 * @method Policial[]    findAll()
 * @method Policial[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PolicialRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Policial::class);
    }
}
