<?php

namespace App\Repository;

use App\Entity\DependentePensionista;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<DependentePensionista>
 *
 * @method DependentePensionista|null find($id, $lockMode = null, $lockVersion = null)
 * @method DependentePensionista|null findOneBy(array $criteria, array $orderBy = null)
 * @method DependentePensionista[]    findAll()
 * @method DependentePensionista[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DependentePensionistaRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DependentePensionista::class);
    }
}
