<?php

namespace App\Repository;

use App\Entity\Usuario;
use App\Model\UsuarioLoginDto;
use App\Model\UsuarioCadastroDto;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Usuario>
 *
 * @method Usuario|null find($id, $lockMode = null, $lockVersion = null)
 * @method Usuario|null findOneBy(array $criteria, array $orderBy = null)
 * @method Usuario[]    findAll()
 * @method Usuario[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UsuarioRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Usuario::class);
    }

    public function verificaUsuario(UsuarioLoginDto $usuarioLoginDto): bool
    {
        if (!$usuario = $this->findOneBy(['apelido' => $usuarioLoginDto->apelido])) {
            return false;
        }

        if (!password_verify($usuarioLoginDto->senha, $usuario->getSenha())) {
            return false;
        }

        return true;
    }

    public function cadastraUsuario(UsuarioCadastroDto $usuarioLoginDto): Usuario
    {

    }
}
