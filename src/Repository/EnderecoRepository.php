<?php

namespace App\Repository;

use App\Entity\Endereco;
use App\Model\EnderecoDto;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Endereco>
 *
 * @method Endereco|null find($id, $lockMode = null, $lockVersion = null)
 * @method Endereco|null findOneBy(array $criteria, array $orderBy = null)
 * @method Endereco[]    findAll()
 * @method Endereco[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EnderecoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Endereco::class);
    }

    /**
     * @param EnderecoDto $enderecoDto Recebe DTO para cadastro ou consulta
     *
     * @return Endereco Retorna apenas um endereço para o cadastro como objeto
     */
    public function registrarEndereco(EnderecoDto $enderecoDto): Endereco
    {
        if ($enderecoEntityManagerResponse = $this->findOneBy(['cep' => $enderecoDto->cep])) {
            return $enderecoEntityManagerResponse;
        }

        $endereco = new Endereco();
        $endereco->setCep($this->limparDados($enderecoDto->cep));
        $endereco->setLogradouro($enderecoDto->logradouro);
        $endereco->setMunicipio($enderecoDto->municipio);
        $endereco->setBairro($enderecoDto->bairro);
        $endereco->setUf($enderecoDto->uf);

        $this->_em->persist($endereco);
        $this->_em->flush();

        return $endereco;
    }

    /**
     * @param string $dado Recebe CEP e retorna somente os numeros em formato de texto
     */
    private function limparDados(string $dado)
    {
        $dado = str_replace("-", "", $dado);

        return $dado;
    }
}
