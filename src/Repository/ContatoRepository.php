<?php

namespace App\Repository;

use App\Entity\Contato;
use App\Model\ContatoDto;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Contato>
 *
 * @method Contato|null find($id, $lockMode = null, $lockVersion = null)
 * @method Contato|null findOneBy(array $criteria, array $orderBy = null)
 * @method Contato[]    findAll()
 * @method Contato[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ContatoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Contato::class);
    }

    /**
     * @param ContatoDto $contatoDto Dados recebidos pelo usuário para registro de contato
     * 
     * @return Contato retorna objeto de contato para cadastro de pessoa
     */
    public function registrarContato(ContatoDto $contatoDto): Contato
    {
        $contato = new Contato();
        $contato->setEmail($contatoDto->email);
        $contato->setTelefoneCelular($contatoDto->telefone_celular);
        $contato->setTelefoneResidencial($contatoDto->telefone_residencial);
        $contato->setTelefoneOutro($contatoDto->telefone_outro);

        $this->_em->persist($contato);
        $this->_em->flush();

        return $contato;
    }
}
