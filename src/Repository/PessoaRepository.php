<?php

namespace App\Repository;

use App\Entity\Pessoa;
use App\Model\PessoaDto;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Exception;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * @extends ServiceEntityRepository<Pessoa>
 *
 * @method Pessoa|null find($id, $lockMode = null, $lockVersion = null)
 * @method Pessoa|null findOneBy(array $criteria, array $orderBy = null)
 * @method Pessoa[]    findAll()
 * @method Pessoa[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PessoaRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Pessoa::class);
    }

    /**
     * @param PessoaDto $pessoaDto Recebe os dados do usuário para popular o objeto Pessoa
     *
     * @return Pessoa
     */
    public function populaPessoa(PessoaDto $pessoaDto): Pessoa
    {
        $pessoa = new Pessoa();
        $pessoa->setNome($pessoaDto->nome);
        $pessoa->setDataNascimento(DateTime::createFromFormat('Y-m-d', $pessoaDto->data_nascimento));
        $pessoa->setCpf($this->limparDados($pessoaDto->cpf));
        $pessoa->setRg($this->limparDados($pessoaDto->rg));
        $pessoa->setRgOrgaoEmissor($pessoaDto->rg_orgao_emissor);
        $pessoa->setRgUf($pessoaDto->rg_uf);
        $pessoa->setSexo($pessoaDto->sexo);
        $pessoa->setComplemento($pessoaDto->complemento);
        $pessoa->setNumero($pessoaDto->numero);
        $pessoa->setEstadoCivil($pessoaDto->estado_civil);

        return $pessoa;
    }

    public function registraPessoa(Pessoa $pessoa): Pessoa|array
    {
        try {
            $this->_em->persist($pessoa);
            $this->_em->flush();
            return $pessoa;
        } catch (Exception $e) {
            throw new Exception("Não foi possível cadastrar pessoa: {$pessoa->getNome()} - CPF: {$pessoa->getCpf()} \n {$e->getMessage()}", 1000001);
        }
    }

    /**
     * @param string $dado Recebe RG ou CPF e retorna somente os numeros em formato de texto
     */
    private function limparDados(string $dado)
    {
        $dado = str_replace(".", "", $dado);
        $dado = str_replace("-", "", $dado);

        return $dado;
    }
}
