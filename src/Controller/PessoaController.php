<?php

namespace App\Controller;

use App\Entity\Pessoa;
use App\Model\ContatoDto;
use App\Model\EnderecoDto;
use App\Model\PessoaDto;
use App\Repository\ContatoRepository;
use App\Repository\EnderecoRepository;
use App\Repository\PessoaRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Attribute\MapRequestPayload;
use Symfony\Component\Routing\Annotation\Route;

class PessoaController extends AbstractController
{
    #[Route('/pessoa', name: 'app_pessoa', methods: ['GET'])]
    public function index(EntityManagerInterface $entityManager): JsonResponse
    {
        $pessoa = $entityManager->getRepository(Pessoa::class)->findAll();

        if (!$pessoa) {
            return $this->json(['status' => 'error', 'message' => 'Não encontrado'], Response::HTTP_NOT_FOUND);
        }

        return $this->json($pessoa);
    }

    #[Route('/pessoa', name: 'create_pessoa', methods: ['POST'])]
    public function create(
    #[MapRequestPayload] PessoaDto $pessoa,
    #[MapRequestPayload] ContatoDto $contato,
    #[MapRequestPayload] EnderecoDto $endereco,
        PessoaRepository $pessoaRepository,
        EnderecoRepository $enderecoRepository,
        ContatoRepository $contatoRepository
    ): JsonResponse {

        // Verifica se o CPF já está cadastrado
        if ($pessoaRepository->findOneBy(['cpf' => $pessoa->cpf])) {
            return $this->json(['status' => 'error', 'message' => 'CPF já cadastrado'], Response::HTTP_BAD_REQUEST);
        }

        $pessoaEntity = new Pessoa();

        // Popula dados do DTO no Entity
        $pessoaEntity = $pessoaRepository->populaPessoa($pessoa);

        // Cria endereço e atribui a Entity
        if ($enderecoEntityManagerResponse = $enderecoRepository->findOneBy(['cep' => $endereco->cep])) {
            $pessoaEntity->setEndereco($enderecoEntityManagerResponse);
        } else {
            $pessoaEntity->setEndereco($enderecoRepository->registrarEndereco($endereco));
        }

        // Cria contato de acordo com o DTO
        $pessoaEntity->setContato($contatoRepository->registrarContato($contato));

        // Registra no banco de dados o cadastro Pessoa
        $pessoaRepository->registraPessoa($pessoaEntity);

        return $this->json(['status'=> 'success','message'=> 'Usuário criado com sucesso'], Response::HTTP_CREATED);
    }
}
