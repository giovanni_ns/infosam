<?php

namespace App\Controller;

use App\Model\UsuarioLoginDto;
use App\Repository\UsuarioRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Attribute\MapRequestPayload;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Csrf\CsrfToken;
use Symfony\Component\Security\Csrf\CsrfTokenManager;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\CsrfTokenBadge;

class TokenController extends AbstractController
{
    #[Route('/token', name: 'app_token', methods: ['POST'])]
    public function index(
    #[MapRequestPayload] UsuarioLoginDto $login,
        UsuarioRepository $usuarioRepository
    ): JsonResponse {

        if ($usuarioRepository->verificaUsuario($login)) {

            $rng = random_int(1200, 2400);
            $token = new CsrfTokenManager();

            return $this->json([
                'token' => [
                    'id' => $token->getToken("$rng")->getId(),
                    'value' => $token->getToken("$rng")->getValue(),
                ]
            ], Response::HTTP_ACCEPTED);
        }

        return $this->json(['message' => ''], Response::HTTP_FORBIDDEN);
    }
}
